// This function encapsulates queries and allows to retry a fixed amount of times.
// Probably should also incorporate logic to call an error function in case a failure happens. 
// The plotStatusFn accepts the retry index and informs about the status. It should also be prepared to plot a message in case of failure, when receiving false as a value.
export async function callAndRetry(fn, maxAmountOfRetries, plotStatusFn) {
    let error;
    for (let retries = 0; retries < maxAmountOfRetries; retries++) {
        if (plotStatusFn) {
            plotStatusFn(retries);
        };
        try {
            return await fn();
        } catch (err) {
            error = err;
            plotStatusFn(false);
        }
    }
    throw error;
};
